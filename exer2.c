#include <stdio.h>
#include <math.h>

int main()
{
  // declare var price, quantity and name
  double price, quan, total, tax;
  char nameOfProduct;
  // input
  printf("please input name: ");
  scanf("%s", &nameOfProduct);

  printf("please input price and quantity: ");
  scanf("%lf %lf", &price, &quan);
  // calculation
  total = price * quan;
  tax = total * 10 / 100;
  // output
  printf("the total is: %lf\n", trunc(total));
  printf("tax is: %lf\n", trunc(tax));

  return 0;
}