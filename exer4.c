#include <stdio.h>
#include <math.h>

int main()
{
  // declare
  int w, h;
  double peri, sa, diagLine;
  // input
  printf("input width and height: ");
  scanf("%d  %d", &w, &h);
  // cal
  peri = 2 * (w + h);
  sa = w * h;
  diagLine = sqrt(pow(w, 2) + pow(h, 2));
  // output
  printf("perimater is: %lf \n", peri);
  printf("surface area is: %lf\n", sa);
  printf("diag line is: %lf \n", diagLine);

  return 0;
}