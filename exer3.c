#include <stdio.h>
#include <math.h>

int main()
{
  double math, lit, chem, avg;
  // input
  printf("input score: \n");
  scanf("%lf %lf %lf", &math, &lit, &chem);
  // cal
  avg = (math + lit + chem) / 3;
  // output
  printf("your average is : %lf", avg);

  return 0;
}