#include <stdio.h>
#include <ctype.h>

int main()
{
  // input
  char input;
  printf("Enter a character: ");
  scanf("%c", &c);
  // logic
  if (isalpha(c))
  {
    printf("%c is a letter of the alphabet\n", c);
  }
  else
  {
    printf("%c is not a letter of the alphabet\n", c);
  }
  return 0;
}