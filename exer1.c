#include <stdio.h>
#include <math.h>

int main()
{
  double numb1, fractpart, intpart;

  // input
  printf("please input numb:  \n");
  scanf("%lf", &numb1);
  // find the int part and the fractpart
  fractpart = modf(numb1, &intpart);
  fractpart = fractpart * 100;
  intpart = (int)numb1;
  // remove decimal places
  fractpart = trunc(fractpart);
  // cal the total
  int total = fractpart + intpart;
  // output
  printf("int part is: %.0f \n", intpart);
  printf("frac part is: %.0f \n", fractpart);
  printf("total is: %d \n", total);

  return 0;
}