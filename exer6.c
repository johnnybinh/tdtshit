#include <stdio.h>
#include <math.h>

int main()
{
  // declare var
  int a, b;
  double x;
  // input
  printf("input a, b: \n");
  scanf("%d %d", &a, &b);
  // cal
  x = (double)-b / (double)a;
  // output
  printf("x is: %lf", x);

  return 0;
}